'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Signup extends Model {
    static get createdAtColumn() {
        return null
    }

    static get updatedAtColumn() {
        return null
    }

    user() {
        return this.belongsTo('App/Models/User', 'user', 'id')
    }

    session()   {
        return this.belongsTo('App/Models/Session', 'session', 'id')
    }
}

module.exports = Signup
