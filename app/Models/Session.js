'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Session extends Model {
    static get createdAtColumn() {
        return null
    }

    static get updatedAtColumn() {
        return null
    }

    static get dates() {
        return super.dates.concat(['sessionDate'])
    }

    static castDates(field, value) {
        if (field === 'sessionDate') {
            return value.format('D MMMM')
        }
    }

    user() {
        return this.belongsTo('App/Models/User')
    }

    signUps() {
        return this.hasMany('App/Models/Signup')
    }
}

module.exports = Session
