'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SessionSchema extends Schema {
  up () {
    this.create('sessions', (table) => {
      table.increments()
      table.timestamps()
      table.date('sessionDate')
      table.integer('tableSize')
      table.integer('dungeonMaster').references('id').inTable('users')
      table.string('sessionName', 128)
      table.string('sessionText', 1024)
    })
  }

  down () {
    this.drop('sessions')
  }
}

module.exports = SessionSchema
