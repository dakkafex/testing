'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SignupSchema extends Schema {
  up () {
    this.create('signups', (table) => {
      table.increments()
      table.integer('session').references('id').inTable('sessions')
      table.integer('user').references('id').inTable('users')
      table.string('userEmail', 100)
      table.boolean('confirmed').defaultTo(false)
    })
  }

  down () {
    this.drop('signups')
  }
}

module.exports = SignupSchema
